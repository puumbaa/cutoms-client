package ru.renue.httpgateway.customs.repository;

import ru.renue.httpgateway.customs.dto.CustomsMessageStatus;

/**
 * Repository for storing messages from customs-service. Should be implemented by the broker.
 */
public interface CustomsMessageRepository {

    /**
     * Saves new messages from customs-service with status "RECEIVED".
     *
     * @param messageId id of the message in customs-service database.
     * @param message xml message as byte array.
     * @param status message status. Should always be "RECEIVED"
     * @throws Exception if any error occurs during saving.
     */
    void save(long messageId, byte[] message, CustomsMessageStatus status) throws Exception;

    /**
     * Updates status of message in database. Used by this library to set status to "COMMITTED"
     * after telling it to the customs-service.
     *
     * @param messageId if of the message in customs-service database.
     * @param status status of the message.
     * @throws IllegalArgumentException if message with specified {@code messageId} not found in
     * database.
     */
    void updateStatus(long messageId, CustomsMessageStatus status) throws IllegalArgumentException;


    /**
     * Returns message status by {@code messageId}. Used in
     * {@link ru.renue.httpgateway.customs.messaging.CustomsServiceMessageCommitter} to commit
     *
     *
     * @param status status of the message in the database.
     * @return id of a message in customs-service.
     */
    long getMessageIdByStatus(CustomsMessageStatus status);
}
