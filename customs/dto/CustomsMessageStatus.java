package ru.renue.httpgateway.customs.dto;

/**
 * Internal status of a message in database.
 */
public enum CustomsMessageStatus {

    RECEIVED("Message was received from customs-service, but not confirmed"),
    COMMITTED("Message receiving was committed");

    private final String description;

    CustomsMessageStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
