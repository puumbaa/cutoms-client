package ru.renue.httpgateway.customs.listener;

/**
 * Manages listener for new messages from customs-service.
 */
public interface CustomsServiceMessageListener {

    /**
     * Starts listening for new messages.
     *
     * @throws Exception if failed to start listening.
     */
    void startListening() throws Exception;

    /**
     * Stops listener.
     *
     * @throws Exception if there are no active listeners.
     */
    void stopListening() throws Exception;
}
