package ru.renue.httpgateway.customs.messaging;

/**
 * Check database for uncommitted messages and sends confirmation for them. Then changes status in
 * the database. Should be a separate thread.
 */
public interface CustomsServiceMessageCommitter {

    /**
     * Starts looking for uncommitted messages in the database and commits them.
     *
     * @throws Exception if failed to start.
     */
    void startLooking() throws Exception;
}
