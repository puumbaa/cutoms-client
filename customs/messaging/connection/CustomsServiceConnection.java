package ru.renue.httpgateway.customs.messaging.connection;

import ru.renue.httpgateway.customs.listener.CustomsServiceMessageListener;
import ru.renue.httpgateway.customs.repository.CustomsMessageRepository;

/**
 * Prepares library for connecting and working with customs-service
 */
public interface CustomsServiceConnection {

    /**
     * Establishes connection to customs-service and returns means to start listening for incoming
     * messages.
     *
     * @param serviceUrl url of customs-service.
     * @param repository broker-implemented repository for saving messages from customs-service.
     * @return starter that allows to initiate listening for messages.
     * @throws Exception if connection to customs-service has failed.
     */
    CustomsServiceMessageListener connect(
            String serviceUrl,
            CustomsMessageRepository repository
    ) throws Exception;
}
