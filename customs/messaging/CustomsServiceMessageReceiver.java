package ru.renue.httpgateway.customs.messaging;

import java.util.List;

/**
 * Handles new messages from customs-service. Receives them from listener, saves them to database,
 * commits them.
 */
public interface CustomsServiceMessageReceiver {

    /**
     * Receives list of message ids, that need to be received, saved and committed.
     *
     * @param messageIdList list of message ids to be processed.
     */
    void processMessages(List<Long> messageIdList);

    /**
     * Message here should be received via REST, saved to the database, and committed. If we saved
     * the message to the database, but failed to commit it - it will be done by
     * {@link CustomsServiceMessageCommitter} later.
     *
     * @param messageId id of a message to be processed.
     * @throws Exception if failed to save to the database or commit to customs-service.
     */
    void saveMessage(long messageId) throws Exception;
}
