package ru.renue.httpgateway;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import net.jcip.annotations.ThreadSafe;

public class Scratch {
    //                          broker-service side

    /**
     * Когда мы хотим отправить сообщение, его id кладется в очередь, а тело вместе с id кладется
     * в кэш. Забирает из кэша только один поток на отправку.
     *
     * UPD: стоит рассмотреть вариант замены MessageCache на обычную ConcurrentHashMap, потому что
     * задача MessageCache сводится к банальной проверке на заполненность кеша при попытке положить
     * в него нову пару.
     */
    @ThreadSafe
    public class MessageCache extends ConcurrentHashMap<UUID, byte[]> {

        // должен чекаться размер кэша, если кэш заполнен, то не кладем в мапу
        @Override
        public byte[] put(UUID key, byte[] value) {
            return super.put(key, value);
        }

        @Override
        public byte[] remove(Object key) {
            return super.remove(key);
        }
    }

    // dto для сообщений на отправку
    public class SendRequest {
        private UUID messageId;
        private byte[] message;
    }

    @ThreadSafe
    public class BrokerMessageProducer {
        BlockingQueue<UUID> queue;
        MessageCache cache;

        public SendRequest prepareToSend(byte[] xmlMessage, UUID envelopeId) {
            // генерируем messageId, пишем все это в базу, формируем SendRequest и отдаем в send(SendRequest sendRequest)
        }

        private void send(SendRequest sendRequest) {
            // messageId кладется в очередь, реквест кладется в кэш (если есть место)
            queue.put(sendRequest.getMessageId());
            cache.put(sendRequest.getMessageId(), sendRequest.getMessage());

            // если много уже в очереди сообщений, то пускай отправляются к брокер-сервису
        }

        // метод для отправки в брокер-сервис (создается RequestSender, который берет группу сообщений
        // и занимается их отправкой и переотправкой.
    }

    // Он занимается отправкой одного запроса и его переотправкой, если не вышло.
    public class RequestSender {
        // он отправляет сообщения и получает коды ответов, если они плохие, то пытается переотправить.
    }


//                           customs-service side

    // паттерн Observer?
    public class ListenerImpl {
        // какой-нибудь метод, который постоянно отправляет гет запрос на список новых сообщений.
        // Получив список, передает его на обработчик (NewMessagesProcessor) и слушает дальше.
    }

    // асинхронно обрабатывает списки, поскольку содержимое списков не пересекается
    public class NewMessagesProcessor {
        public void processMessages(List<Long> messageIdList) {
            // для каждого сообщения из списка вызывает метод save.
        }

        public void saveMessage(long messageId) {
            // по айдишнику делает рест запрос на кастомс сервис и сохраняет результат в репозиторий.
        }
    }

    // Ходит в репозиторий, ищет сообщения со статусом RECEIVED, подтвержадет их получение по ресту,
    // меняет статус сообщения в базе. Работает фоновым потоком.
    public class MessageCommitter {
        public void commitMessage(long messageId) {
            // делает рест запрос на кастомс-сервис по айди, после чего меняет статус в базе.
        }
    }
}
